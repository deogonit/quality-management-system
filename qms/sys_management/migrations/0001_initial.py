# Generated by Django 2.2 on 2019-12-15 11:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('position', models.CharField(choices=[('IN', 'Intern'), ('JR', 'Junior Software Developer'), ('MD', 'Software Developer'), ('SR', 'Senior Developer'), ('PM', 'Project Manager'), ('QA', 'QA Engineer'), ('BA', 'Business Analyst')], max_length=2)),
                ('department', models.CharField(choices=[('D1', 'First department'), ('D2', 'Second department'), ('D3', 'Third department'), ('D4', 'Fourth department')], max_length=2)),
                ('status', models.BooleanField(default=True)),
                ('slug', models.SlugField(blank=True, unique=True)),
            ],
        ),
    ]
