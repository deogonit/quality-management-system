from django.apps import AppConfig


class SysManagementConfig(AppConfig):
    name = 'sys_management'
