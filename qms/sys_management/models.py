from django.db import models
from .utils import unique_slug_generator
from django.db.models.signals import pre_save
from django.shortcuts import reverse


class Employee(models.Model):
    DEPARTMENT_CHOICES = [
        ("D1", "First department"),
        ("D2", "Second department"),
        ("D3", "Third department"),
        ("D4", "Fourth department"),
    ]

    POSITION_CHOICES = [
        ("IN", "Intern"),
        ("JR", "Junior Software Developer"),
        ("MD", "Software Developer"),
        ("SR", "Senior Developer"),
        ("PM", "Project Manager"),
        ("QA", "QA Engineer"),
        ("BA", "Business Analyst"),
    ]

    name = models.CharField(max_length=30)
    position = models.CharField(max_length=2, choices=POSITION_CHOICES)
    department = models.CharField(max_length=2, choices=DEPARTMENT_CHOICES)
    status = models.BooleanField(default=True)
    slug = models.SlugField(unique=True, blank=True)

    def __str__(self):
        return self.name

    def get_edit_url(self):
        return reverse('edit_employee', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('delete_employee', kwargs={'slug': self.slug})


def pre_save_slug(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance, instance.name, instance.slug)


pre_save.connect(pre_save_slug, sender=Employee)
