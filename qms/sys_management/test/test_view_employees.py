from django.test import TestCase
from django.urls import reverse, resolve

from ..models import Employee
from ..views import EmployeesView


class EmployeeTests(TestCase):
    def setUp(self) -> None:
        self.employee = Employee.objects.create(name='Test name', position='IN', department='D1')

    def test_employee_view(self):
        response = self.client.get(reverse('employees'))
        self.assertEquals(response.status_code, 200)

    def test_employee_view_uses_correct_template(self):
        response = self.client.get(reverse('employees'))
        self.assertTemplateUsed(response, 'sys_management/employees.html')

    def test_employee_view_function(self):
        view = resolve('/qms/employees')
        self.assertEquals(view.func.view_class, EmployeesView)