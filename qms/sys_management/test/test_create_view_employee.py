from django.test import TestCase
from django.urls import reverse, resolve
from django.contrib.auth.models import User

from ..models import Employee
from ..forms import CreateNewEmployeeForm


class NewEmployeeTests(TestCase):
    def setUp(self) -> None:
        User.objects.create_user(username='name', email='name@name.com', password='password')
        self.client.login(username='name', password='password')

    def test_new_employee_view_success_status_code(self):
        url = reverse('create_employee')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_csrf(self):
        url = reverse('create_employee')
        response = self.client.get(url)
        self.assertContains(response, 'csrfmiddlewaretoken')

    def test_new_employee_contains_form(self):
        url = reverse('create_employee')
        response = self.client.get(url)
        form = response.context.get('form')
        self.assertIsInstance(form, CreateNewEmployeeForm)

    def test_new_employee_invalid_data(self):
        url = reverse('create_employee')
        data = {}
        response = self.client.post(url, data)
        form = response.context.get('form')
        self.assertEquals(response.status_code, 200)
        self.assertTrue(form.errors)

    def test_new_employee_valid_data(self):
        url = reverse('create_employee')
        data = {
            'name': 'Name',
            'position': 'IN',
            'department': 'D1'
        }
        self.client.post(url, data)
        self.assertTrue(Employee.objects.exists())

    def test_new_employee_empty_fields(self):
        url = reverse('create_employee')
        data = {
            'name': '',
            'position': '',
            'department': ''
        }
        response = self.client.post(url, data)
        self.assertEquals(response.status_code, 200)
        self.assertFalse(Employee.objects.exists())
