from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

from ..models import Employee


class EditEmployeeViewTestCase(TestCase):
    def setUp(self) -> None:
        self.employee = Employee.objects.create(name='Name', position='IN', department='D1')
        User.objects.create_user(username='name', email='name@name.com', password='password')
        self.url = reverse('edit_employee', kwargs={
            'slug': self.employee.slug,
        })


class EditEmployeeViewTests(EditEmployeeViewTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEquals(self.response.status_code, 302)


class SuccessfulEditEmployeeViewTests(EditEmployeeViewTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client.login(username='name', password='password')
        data = {
            'name': 'maers',
            'position': 'IN',
            'department': 'D1',
            'status': True
        }
        self.response = self.client.post(self.url, data)

    def test_redirection(self):
        topic_post_url = reverse('employees')
        self.assertRedirects(self.response, topic_post_url)

    def test_employee_name_changed(self):
        self.employee.refresh_from_db()
        self.assertEquals(self.employee.name, 'maers')


class InvalidEditEmployeeViewTests(EditEmployeeViewTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client.login(username='name', password='password')
        data = {}
        self.response = self.client.post(self.url, data)

    def test_redirection(self):
        self.assertEquals(self.response.status_code, 200)

    def test_form_errors(self):
        form = self.response.context.get('form')
        self.assertTrue(form.errors)
