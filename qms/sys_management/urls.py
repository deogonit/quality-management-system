from django.urls import path

from .views import (EmployeesView, CreateNewEmployeeView, EditEmployeeView, DeleteEmployeeView)

urlpatterns = [
    path('', EmployeesView.as_view(), name='employees'),
    path('employee/create-employee', CreateNewEmployeeView.as_view(), name='create_employee'),
    path('employee/edit-employee/<str:slug>/', EditEmployeeView.as_view(), name='edit_employee'),
    path('employee/delete-employee/<str:slug>/', DeleteEmployeeView.as_view(), name='delete_employee'),
]