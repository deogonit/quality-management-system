from django import forms

from .models import Employee


class CreateNewEmployeeForm(forms.ModelForm):
    name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Enter your name'
            }
        )
    )
    blank_choice = [('', 'Enter value'), ]
    position = forms.ChoiceField(
        choices=blank_choice + Employee.POSITION_CHOICES,
        widget=forms.Select(
            attrs={
                'class': 'form-control',
            }
        )
    )
    department = forms.ChoiceField(
        choices=blank_choice + Employee.DEPARTMENT_CHOICES,
        widget=forms.Select(
            attrs={
                'class': 'form-control',
            }
        )
    )

    class Meta:
        model = Employee
        fields = [
            'name',
            'position',
            'department'
        ]


class EditEmployeeForm(forms.ModelForm):
    name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )
    position = forms.ChoiceField(
        choices=Employee.POSITION_CHOICES,
        widget=forms.Select(
            attrs={
                'class': 'form-control',
            }
        )
    )
    department = forms.ChoiceField(
        choices=Employee.DEPARTMENT_CHOICES,
        widget=forms.Select(
            attrs={
                'class': 'form-control',
            }
        )
    )

    status = forms.BooleanField(
        initial=True,
        required=False,
        label='Active/Inactive'
    )

    class Meta:
        model = Employee
        fields = [
            'name',
            'position',
            'department',
            'status'
        ]
