from django.shortcuts import render, redirect, get_object_or_404
from django.views import View
from django.core.paginator import Paginator
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.http import JsonResponse

from .models import Employee
from .forms import (CreateNewEmployeeForm, EditEmployeeForm)


class EmployeesView(View):
    template_name = 'sys_management/employees.html'

    def get(self, request, *args, **kwargs):
        employees = Employee.objects.all().order_by('-pk')
        paginator = Paginator(employees, 10)
        page_number = request.GET.get('page', 1)
        employees = paginator.get_page(page_number)
        context = {
            'page_objects': employees,
            'is_authenticated': request.user.is_authenticated
        }
        return render(request, self.template_name, context)


@method_decorator(login_required, name='dispatch')
class CreateNewEmployeeView(View):
    template_name = 'sys_management/create_employee.html'

    def get(self, request, *args, **kwargs):
        form = CreateNewEmployeeForm()
        context = {
            'form': form
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = CreateNewEmployeeForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('employees')
        context = {'form': form}
        return render(request, self.template_name, context)


@method_decorator(login_required, name='dispatch')
class EditEmployeeView(View):
    template_name = 'sys_management/edit_employee.html'

    def get(self, request, *args, **kwargs):
        employee = get_object_or_404(Employee, slug=kwargs.get('slug'))
        bound_form = EditEmployeeForm(instance=employee)
        context = {
            'form': bound_form,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        employee = get_object_or_404(Employee, slug=kwargs.get('slug'))
        form = EditEmployeeForm(request.POST, instance=employee)
        if form.is_valid():
            form.save()
            return redirect('employees')
        context = {
            'form': form
        }
        return render(request, self.template_name, context)


class DeleteEmployeeView(View):
    def get(self, request, *args, **kwargs):
        employee = get_object_or_404(Employee, slug=kwargs.get('slug'))
        data = dict()
        context = {
            'employee': employee
        }
        data['html_form'] = render_to_string(template_name='sys_management/partial_employee_delete.html',
                                             context=context, request=request)
        return JsonResponse(data)

    def post(self, request, *args, **kwargs):
        employee = get_object_or_404(Employee, slug=kwargs.get('slug'))
        data = dict()
        employee.delete()
        data['form_is_valid'] = True
        employees = Employee.objects.all().order_by('-pk')
        paginator = Paginator(employees, 10)
        page_number = request.GET.get('page', 1)
        employees = paginator.get_page(page_number)
        data['html_employee_list'] = render_to_string('sys_management/list_employees.html', {
            'page_objects': employees,
            'user': request.user
        })
        return JsonResponse(data)
