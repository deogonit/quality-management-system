# Quality-management-system

[![Python Version](https://img.shields.io/badge/python-3.7-blue)](https://python.org)
[![Django Version](https://img.shields.io/badge/django-2.2-yellow)](https://djangoproject.com)

Web-site that allows user create, delete and manage your employees.

## Running the Project Locally
First, clone the repository to your local virtual environment:

`git clone https://deogonit@bitbucket.org/deogonit/quality-management-system.git`

Install the requirements:

`pip install -r requirements.txt`

Create the database:

`python manage.py migrate`

Finally, run the development server:

`python manage.py runserver`

The project will be available at 127.0.0.1:8000.